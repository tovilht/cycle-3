# Cycle Three

# ANN

## Experimental

1. the same dataset that was used to train the SVM in cycle-2 was reused
2. five types of DNA was present:
   1. 1 KB
   2. 5 KB
   3. 10 KB
   4. 20 KB
   5. 48.5KB
3. The last result for Matlab Fine Gaussian SMV = 83%, Python SVM = 78.8%

## Technicals

1. the independent variables were standardised while the dependent variables were labeled
2. the ann constructed has a structure of:

```python
import tensorflow as tf
ann = tf.keras.models.Sequential()
ann.add(tf.keras.layers.Dense(units=6, activation='relu'))
ann.add(tf.keras.layers.Dense(units=6, activation='relu'))
ann.add(tf.keras.layers.Dense(units=5, activation='softmax'))
ann.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics = ['accuracy'])
ann.fit(X_train, y_train, batch_size = 32, epochs = 100)
```

## Result

1. The accuracy of the ANN for classifying data that was manually extracted from the nanopore app converges to that of using SMV: ~80%
2. It can be concluded that the maximal degree of accuracy of using peak picking and manually tuned parameters to analysis the DNA is around 80%

# Binary 1D CNN

## General Experimental

1. Five datasets were used for each type of DNA, while the following were compares:
   1. 5KB vs 20KB
   2. 5KB vs 48.5KB
   3. 20KB vs 48.5KB
2. Each dataset of a DNA has 12 million datapoints, hence each set were splitted into 1,000 segments with 12,000 datapoints in each of the segments for analysis

## Trial One

### Experimental

1. the segments were extracted, with the independent variables normalized and the dependent onehot encoded. No mathematical operations were performed to the segments.
2. the data were shuffled and fed into a neural network with two hidden layers and 32 neurons.
3. The CNN was allowed to run 50 epochs

### Result

1. the training accuracy increased from 50% to 98%
2. but the validation accuracy remained at 50%
3. the model overfitted very seriously to the training data, learning the pattern of each segments but were unable to classify unseen data

### Conclusion

1. model fail to predict the data, the accuracy was the same to flipping a coin
2. overfitting is severe hence should be improved.

## Trial Two

### Experimental

1. Since baseline correction was different for different types of DNA, i.e. smaller DNA will use a smaller baseline and longer DNA will use a longer baseline. Hence, **baseline correction was not a feasible idea.**
2. **extracting the peaks from the segments was not a feasible idea as well**, reasons:
   1. the threshold for peak extraction can vary for different DNA data, so human factors will be involved
   2. this is not sustainable, since the model should be able to classify different segements of a mixed DNA. however, if peak picking was used, there will be areas where no peaks were detected so the model doesnt know which DNA does that segment belongs to.
3. **Fast fourier transform** was applied to each segment to extract the signals. Since we are not concerned with the time domain, i.e. we do not care about when the segment will appear, we just care about which DNA does that segment belongs to.
   1. FFT will extract the frequency of the data from the segment and hence produces the **fingerprint** of the DNA:
      ![FFT](http://pic3.zhimg.com/bb8de9d8a622ec08852a334ed34f404b_b.jpg) https://daily.zhihu.com/story/3935067
   2. the data being fourier transformed will be normalized to the same scale such the the maximum of the data will have a value of 1
   3. hence the data extracted from different DNA sequence with different features can be compared on the same scale
4. The binary CNN model:

```python
from tensorflow import keras

# input shape
input_shape=(X_train.shape[1], 1)

# build network topology
model = keras.Sequential()

# 1st conv layer
model.add(keras.layers.Conv1D(32, 3, activation='relu', input_shape=input_shape))
model.add(keras.layers.MaxPooling1D(3, strides=2, padding='same'))
model.add(keras.layers.BatchNormalization())

# 2nd conv layer
model.add(keras.layers.Conv1D(32, 3, activation='relu'))
model.add(keras.layers.MaxPooling1D(3, strides=2, padding='same'))
model.add(keras.layers.BatchNormalization())
model.add(keras.layers.Dropout(0.1))

# 3rd conv layer
model.add(keras.layers.Conv1D(32, 2, activation='relu'))
model.add(keras.layers.MaxPooling1D(2, strides=2, padding='same'))
model.add(keras.layers.BatchNormalization())
model.add(keras.layers.Dropout(0.1))

# flatten output and feed it into dense layer
model.add(keras.layers.Flatten())
model.add(keras.layers.Dense(64, activation='relu'))
model.add(keras.layers.Dropout(0.3))

# output layer
# specifing the number of outputs and the activation function
model.add(keras.layers.Dense(1, activation='sigmoid'))
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
model.summary()
```

### Result

1. the learning accuracy was achieved to 95% at around 20 epochs
2. ![plot](Binary CNN/images/plot.png)
3. the accuracy on the testing set was 97.7% with 5 KB vs 20 KB
4. the overfitting problem was removed with
   1. Fourier Transformation that extracts the fingerprints from the DNA
   2. the Dropout layer reduces overfitting
5. The model was successful in the learning the features of the fingerprints in the DNA and can classify each segment based on their features

### Advantages

1. no human interference was required to analyse the DNA. The segments were transformed into signals by fast fourier transform
2. no peak detection was required and hence there wont be empty spaces that the model cant predict or label.
3. automation in analysis since no manual work is required to tune parameters.
